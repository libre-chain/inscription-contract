# Inscription Contract
This contract allows domain owners to set the markdown content of their website.

## Rules
- The domain must be owned by the sender.
- Only the owner can set the content.

## Prerequisites

### Mac M1/M2 (Apple Silicon)
- [Docker](https://www.docker.com/): build the contract using Docker.

### Ubuntu 20.04
- [clsdk](https://github.com/gofractally/contract-lab): build the contract using clsdk.
- (optional) [Docker](https://www.docker.com/): build the contract using Docker.

## Getting Started
To build the contract, there are two main files to consider:
1. `./build.sh`: build the contract wihout docker. This options requires to follow the installation steps of `clsdk` that you can find [here](https://github.com/gofractally/contract-lab).
2. `./docker_build.sh`: build the contract with docker.


## Build
On every compilation, the contract is built in the `./build` folder and the resulting `.wasm` and `.abi` files are copied to the `inscriptions` folder. Also, at the end of the building process, the `sha256` value for the `.wasm` file is displayed.

```bash
WASM hash: 3362d757437180a6db626926a9e8fa31d6a37ea36012e70b210558dd4108d804  build/inscriptions.wasm
```

## Cleos Commands
### Set Content
```bash
cleos -u <url> push action inscriptions setcontent '{"id": <number>, "content": "<string>", "tmplate": "<string>"}' -p <domain_owner>@active
```

### Remove Content
```bash
cleos -u <url> push action inscriptions remove '{"id": <number>}' -p <domain_owner>@active
```

### Flag Content
```bash
cleos -u <url> push action inscriptions flag '{"id": <number>, "flag": <boolean>}' -p <domain_owner>@active
```

> Note: Replace every placeholder with this format <key> to the corresponding value.