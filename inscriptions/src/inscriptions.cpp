#include <constants.hpp>
#include <inscriptions.hpp>
#include <ordinals.hpp>

namespace insc {

  void inscriptions::setcontent( uint64_t     id,
                                 std::string &content,
                                 std::string &tmplate ) {
    ordinals::inscriptions_table inscriptions_tb( ORDINALS_CONTRACT,
                                                  ORDINALS_CONTRACT.value );
    auto                         insc_itr = inscriptions_tb.find( id );

    eosio::check( content.size() + tmplate.size() > 0,
                  "content and template must not be empty" );
    eosio::check( content.size() <= MAX_BYTES_SIZE,
                  "content must not be greater than " +
                      std::to_string( MAX_BYTES_SIZE ) + " bytes" );
    eosio::check( tmplate.size() <= MAX_BYTES_SIZE,
                  "template must not be greater than " +
                      std::to_string( MAX_BYTES_SIZE ) + " bytes" );
    eosio::check( insc_itr != inscriptions_tb.end(),
                  "domain name not found in ordinals contract" );

    require_auth( insc_itr->owner );

    auto content_itr = contents_tb.find( id );

    if ( content_itr == contents_tb.end() ) {
      contents_tb.emplace( insc_itr->owner, [&]( auto &row ) {
        row.id() = id;
        row.domain() = insc_itr->domain;

        if ( content.size() > 0 ) {
          row.markdown() = content;
        }

        if ( tmplate.size() > 0 ) {
          row.tmplate() = tmplate;
        }
      } );
    } else {
      contents_tb.modify( content_itr, insc_itr->owner, [&]( auto &row ) {
        if ( content.size() > 0 ) {
          row.markdown() = content;
        }

        if ( tmplate.size() > 0 ) {
          row.tmplate() = tmplate;
        }
      } );
    }
  }

  void inscriptions::remove( uint64_t id ) {
    ordinals::inscriptions_table inscriptions_tb( ORDINALS_CONTRACT,
                                                  ORDINALS_CONTRACT.value );
    auto                         insc_itr = inscriptions_tb.find( id );

    eosio::check( insc_itr != inscriptions_tb.end(),
                  "domain name not found in ordinals contract" );

    require_auth( insc_itr->owner );

    auto itr = contents_tb.find( id );
    eosio::check( itr != contents_tb.end(), "content not found" );

    contents_tb.erase( itr );
  }

  void inscriptions::flag( uint64_t id, bool flag ) {
    require_auth( get_self() );

    update_status( id, flag ? status_content::active : status_content::banned );
  }

  void inscriptions::update_status( uint64_t id, status_content new_status ) {
    auto itr = contents_tb.find( id );
    eosio::check( itr != contents_tb.end(), "content not found" );

    contents_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
      row.status() = new_status;
    } );
  }

  void inscriptions::clearall( uint32_t max_steps ) {
    require_auth( get_self() );

    for ( auto itr = contents_tb.begin();
          itr != contents_tb.end() && 0 < max_steps;
          --max_steps ) {
      itr = contents_tb.erase( itr );
    }
  }
} // namespace insc

EOSIO_ACTION_DISPATCHER( insc::actions )

EOSIO_ABIGEN( actions( insc::actions ),
              table( "contents"_n, insc::content_variant ) )