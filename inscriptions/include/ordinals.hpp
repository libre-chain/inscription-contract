#include <eosio/eosio.hpp>

namespace ordinals {
  struct inscriptions {
    uint64_t    id;
    eosio::name owner;
    std::string domain;
    std::string inscription_id;
    uint64_t    inscription_number;
    std::string inscribed_at;
    uint64_t    primary_key() const { return id; }
  };

  EOSIO_REFLECT( inscriptions,
                 id,
                 owner,
                 domain,
                 inscription_id,
                 inscription_number,
                 inscribed_at )
  using inscriptions_table =
      eosio::multi_index< "inscriptions"_n, inscriptions >;
} // namespace ordinals