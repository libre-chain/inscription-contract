#include <eosio/crypto.hpp>
#include <eosio/eosio.hpp>

#include <utils.hpp>

#define BOOST_NO_EXCEPTIONS

namespace insc {

  enum status_content : uint8_t { banned = 0, active = 1 };

  struct content_v0 {
    uint64_t    id;
    std::string domain;
    std::string markdown;
    std::string tmplate;
    uint8_t     status = status_content::active;

    uint64_t primary_key() const { return id; }
    uint64_t by_status() const { return static_cast< uint64_t >( status ); }
    eosio::checksum256 by_domain() const {
      return eosio::sha256( domain.data(), domain.size() );
    }
  };
  EOSIO_REFLECT( content_v0, id, domain, markdown, tmplate, status )

  using content_variant = std::variant< content_v0 >;

  struct content {
    content_variant value;

    FORWARD_MEMBERS( value, id, domain, markdown, tmplate, status )
    FORWARD_FUNCTIONS( value, primary_key, by_status, by_domain )
  };
  EOSIO_REFLECT( content, value )

  using content_table = eosio::multi_index<
      "contents"_n,
      content,
      eosio::indexed_by<
          "bystatus"_n,
          eosio::const_mem_fun< content, uint64_t, &content::by_status > >,
      eosio::indexed_by< "bydomain"_n,
                         eosio::const_mem_fun< content,
                                               eosio::checksum256,
                                               &content::by_domain > > >;

  class inscriptions : public eosio::contract {
  private:
    content_table contents_tb;

  public:
    using eosio::contract::contract;

    inscriptions( eosio::name                       receiver,
                  eosio::name                       code,
                  eosio::datastream< const char * > ds )
        : contract( receiver, code, ds ),
          contents_tb( receiver, receiver.value ) {}

    void setcontent( uint64_t id, std::string &content, std::string &tmplate );
    void remove( uint64_t id );
    void flag( uint64_t id, bool flag );
    void clearall( uint32_t max_steps );

    void update_status( uint64_t id, status_content new_status );
  };

  EOSIO_ACTIONS( inscriptions,
                 "inscriptions"_n,
                 action( setcontent, id, content, tmplate ),
                 action( remove, id ),
                 action( flag, id, flag ),
                 action( clearall, max_steps ) )

} // namespace insc