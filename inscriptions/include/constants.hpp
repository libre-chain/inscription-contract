#include <eosio/name.hpp>

namespace insc {
  inline constexpr eosio::name ORDINALS_CONTRACT = "ordinals"_n;
  inline constexpr size_t      MAX_BYTES_SIZE = 100'000;

} // namespace insc