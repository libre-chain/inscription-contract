set -e

mkdir -p build
cd build
cmake `clsdk-cmake-args` ..
make -j $(nproc)
ctest -j10 -V
cd ..

cp build/inscriptions.* ./inscriptions/  || true

echo "WASM hash: $(sha256sum inscriptions/inscriptions.wasm)"